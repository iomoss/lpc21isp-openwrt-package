# lpc21isp

Lpc21isp is a portable command line ISP (In-circuit Programmer) for NXP LPC
family and Analog Devices ADUC70xx.

This repository holds a Makefile for building it on OpenWRT.

# Usage

To use this Makefile;

* Acquire the OpenWRT-SDK for the specific architecture.

See: https://wiki.openwrt.org/doc/howto/obtain.firmware.sdk

* Decompress the SDK and enter the packages sub-directory.

```bash
cd package
```

* Clone this repository.

```bash
git clone git@bitbucket.org:iomoss/lpc21isp-openwrt-package.git
```

After this the directory structure should be like this;
```
lpc21isp/
Makefile
```

* Go to the root of the SDK

```bash
cd ..
```

* Start building the package

```bash
make
```

* Assuming everything went well; pickup the package and use it

The package should be located in;
```
/bin/{{TARGET_SYSTEM}}/packages/base/lpc21isp_197-1_{{TARGET_SYSTEM}}_{{ARCH}}.ipk
```
For the LinkIt Smart 7688 (ramips) this becomes;
```
/bin/ramips/packages/base/lpc21isp_197-1_ramips_24kec.ipk
```
